#!/usr/bin/env bash

read n
sum=0
i=0
while [[ $i -lt $n ]]; do
    read num
    sum=$((sum+num))
    i=$((i+1))
done
printf "%.3f\n" $(echo "$sum / $n" | bc -l)