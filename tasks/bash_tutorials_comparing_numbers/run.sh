#!/usr/bin/env bash

read x
read y
if [[ $x -gt $y ]]
then
    echo -ne 'X is greater than Y'  
elif [[ $x -lt $y ]]
then
    echo -ne 'X is less than Y'
else
    echo -ne 'X is equal to Y'
fi